#include "user_tools.h"
#include "hash.h"
#include "user.h"
/*
From what I can see from previous code, the max sizes for usernames and passwords are 8 and 32 respectively.
*/
//assumes users stored in order of uid
int nextUID() {
	char * filename = "users";
	int fd = open(filename,2);

	char uid;
	char junk[80];

	int max = 0;
	int bytes_read = -1;

	while(bytes_read != 0){
		read(fd,&uid,1);
		
		if (uid >= max) {
			max = uid + 1;
		}

		bytes_read = read(fd,junk,79);
	}

	close(fd);
	return max;
}

int write_user(int _uid, int _gid, char* username, char* password, char* salt) {
	char * c = ",";
	char* end = "\n\0";

	char uid[2];
	uid[0] =  _uid;

	//default group id for now 1
	char gid[2];
	gid[0] = gid[0] + _uid;

	//file i/0
	char * filename = "users";
	int fd = open(filename,O_RDWR);

	int written = 0;
	char rd_buff[80];
	int i = 0;

	while(i < _uid){
		read(fd,rd_buff,80);
		i++;
	}

	written += write(fd,uid,1);
	written += write(fd,c,1);
	written += write(fd,username,8);
	written += write(fd,c,1);
	written += write(fd,password,32);
	written += write(fd,c,1);

	written += write(fd,salt,32);
	written += write(fd,c,1);

	written += write(fd,gid,1);
	written += write(fd,end,2);
	close(fd);

	if (written != 80){ return 1; }

	return 0;
}

void cleanUser(char* username) {

	for (int x = strlen(username); x < 8; x++){
		username[x] = ' ';
	}
	username[8] = 0;
}

void cleanPassword(char *password) {
	int changing = 0;
	for (int i = 0; i < 32; i++) {
		if (password[i] == '\n') {
			changing = 1;
		}
		if (changing) {
			password[i] = 0;
		}
	}

	password[32] = 0;
}

int main(int argc, char  *argv[]) {
  char password[35];

	//if root***
	if(0 == getuid()){
    if (argc < 3) {
      printf(1, "Need more args.\n usage: adduser username password\n");
      return 0;
    }

    printf(1,"***Add User***\n");

    // Prepping the username
    char username[11];
    if (strlen(argv[1]) > 8) {
      printf(1, "Username cannot be longer than 8 characters. Try again.\n");
		return 0;
    }
    int i =11;
    while (i--) {
      username[i] = ' ';
    }
    strcpy(username, argv[1]);
    cleanUser(username);
    // Done w/ the username

    // Prepping the password
    i = 35;
    while (i--) {
      password[i] = 0;
    }

    if (strlen(argv[2]) > 32) {
      printf(1, "password cannot be longer than 32 characters. Try again.\n");
      return 0;
    }
    strcpy(password, argv[2]);
    cleanPassword(password);
    // Done w/ the password

		//checks duplicate username
		if(chk_username(username) != -1){
			printf(1,"Username is already taken!\n");
			return 0;
		}

		//find next UID
		int uid = nextUID();

		//set group id???
		int gid = 1;

		//hash password
		char salt[32];
		char hashed_pass[32];

		getSalt(salt);
		hashPassword(password, salt, hashed_pass);

		if (write_user(uid,gid,username,hashed_pass,salt) != 0) {
			printf(1,"ERROR: User not Added!\n");
		} else {
			printf(2,"User Added!\n");
		}
    print_users();
	} else {
		printf(1,"Must have root access!\n");
	}
	clearPassword(password);
	return 0;
}
