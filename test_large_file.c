#include "types.h"
#include "stat.h"
#include "user.h"
#include "fcntl.h"


int
main(int argc, char *argv[])
{
  	int fd;
	fd = open("max_test.txt", O_CREATE | O_RDWR);
	if(fd < 0){
		printf(1, "Open failed\n");
		exit();
	}

	//Allocate enough space for one disk block
	char buf[512];

	//Use dev/rand to get junk to fill test file
	int ran = open("/dev/rand", O_RDONLY);
	if(ran < 0){
		printf(1, "Open failed.\n");
		exit();
	}

	//Write 281 blocks to the file
	//This is the maximum number of blocks allowable per file
	//26 Direct Blocks + 127 Indirect blocks + 128 Indirect blocks
	for(int i = 0; i < 281; i++){
		if(read(ran, buf, 512) < 0){
			printf(1, "Read failed.\n");
			exit();
		}
		if(write(fd, buf, 512) < 0){
			printf(1, "Write failed on write %d\n", i);
			exit();
		}
	}
	close(fd);
	unlink("max_test.txt");
	exit();
}
