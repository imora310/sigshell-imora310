#include "types.h"
#include "user.h"
#include "fcntl.h"
#include "user_tools.h"
int
main(int argc, char *argv[])
{
  //Declaring an array to hold the name of the program and its arguments.
  char *execargs[argc -1];
  //Need to prompt for the user password before running
  char password[35];
  //Going to need to grab the user's ID
  int uid = -1;

  //Doing this so "sudo" doesn't end up being in the arguments list. There may be a better way to do this.
  int i;
  for (i = 1; i < argc; i++) {
    execargs[i-1] = argv[i];
  }
  //prompting for pass to check against their username
  get_password(password);
  //returns my uid. if not root, it changes it to root temporarily so i can exec the program.
  uid = ssudo();
  if (check_pass(uid,password) != 1) {

  printf(1, "Password does not match. Exiting.\n");
  return 0;
  }

//just calling exec
exec(execargs[0], execargs);
//exec shouldn't return. if we hit this, we fail.
printf(1, "Exec failed.\n");
return 0;

}
