#include "user_tools.h"
#include "hash.h"


void exitWithMessage(char * msg) {
	printf(1, "%s", msg);
	exit();
}

int write_pass(int uid, char* password, char* salt){
	int fd = open("users",O_RDWR);
	char buf[80];

	for(int i = 0; i < uid; i++)
		read(fd,buf,80);

	read(fd,buf,11);
	if(write(fd,password,32) == 32) {
		read(fd,buf,1);
		if(write(fd,salt,32) == 32) {
			close(fd);
			return 1;
		}
	}

	close(fd);
	return 0;
}

/* changepassword takes 0 or 1 arguments
 * if no argument given, it changes the password of the user logged in
 * if given a username as an argument, root can change any password
*/
int main(int argc, char const *argv[])
{
	printf(1,"***Change Password***\n");
	
	char username[11] = "        \0  ";
	char old_pass[35];
	int uid = getuid(); // current user
	int givenUid; 		// given user

	// if they offered a username
	if (argc > 1) {
		int usrLen = 0;
		while(argv[1][usrLen] != '\0') { usrLen++; }
		
		if (usrLen > 8)
			exitWithMessage("Username cannot be longer than 8 characters. Try again.\n");

		// get uid from username argument
		for(int i = 0; i < usrLen; i++)
			username[i] = argv[1][i];
		givenUid = chk_username(username);

		if(givenUid == -1)
			exitWithMessage("User not Found!\n");

		// if not root and not changing their own pwd, fail
		if (uid != 0 && uid != givenUid)
			exitWithMessage("Action Denied: You may only change your own password.\n");

		// root: assign uid of the given user
		uid = givenUid;
	}

	for(int i = 0; i < 4; i++) {
		printf(1,"***Old Password***\n");
		get_password(old_pass);

		if(check_pass(uid, old_pass) == 0) {
			printf(1,"Passwords do not match\n");
		} else {
			//changepass
			printf(1,"***New Password***\n");
			get_password(old_pass);

			//hash password
			char salt[32];
			char hashed_pass[32];

			getSalt(salt);
			hashPassword(old_pass, salt, hashed_pass);

			if(write_pass(uid, hashed_pass, salt) == 1)
				exitWithMessage("Password Changed!\n");
			else
				exitWithMessage("ERROR: Password Not Changed!\n");
		}
	}
	exitWithMessage("Allowed Attempts Exausted\n");
}
